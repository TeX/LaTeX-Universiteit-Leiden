\documentclass[11pt,a4paper,dutch]{article}
\usepackage[utf8]{inputenc}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{fancyhdr,babel,textcomp,eurosym,color,listings,hyperref,graphicx,subfigure,pgffor,geometry,grffile}
\usepackage[style]{ULtitle}

\lstset{	language=[LaTeX]TeX,	basicstyle=\tt,numbers=left,numberstyle=\tiny\color{gray}\tt,	tabsize=4,captionpos=b,breaklines=true,keywordstyle=\color{blue},morekeywords={subtitle,course,faculty,maketitle,supervisor},xleftmargin=.25in,xrightmargin=.25in}
\hypersetup{colorlinks,citecolor=black,filecolor=black,linkcolor=black,urlcolor=black}
\DeclareUnicodeCharacter{20AC}{\euro}

\title{ULtitle}
\subtitle{Universiteit Leiden titels voor \LaTeX}
\author{Silke Hofstra}

\begin{document}
\maketitle
Dit document beschrijft de installatie en het gebruik van het \emph{ULtitle} pakket voor \LaTeX.
Het pakket voorziet twee vervangende titels voor \LaTeX met het zegel van de Universiteit Leiden.
\setcounter{tocdepth}{2}
\tableofcontents

\newpage
\lhead{\textsc{ULtitle}}
\rhead{\fontfamily{fxl}\selectfont \textsc{\nouppercase{ \leftmark}}}
\pagestyle{fancy}

\section{Introductie}
\subsection{Huisstijl}
Het document kan gezet worden in de huisstijl door de \texttt{style} of \texttt{sans} optie te gebruiken. Zie ook~\ref{sec:typeface} en~\ref{sec:opt}.
\subsection{Lettertype}
\label{sec:typeface}
De officiële lettertypen die in drukwerk worden gebruikt worden door de universiteit als volgt uitgelegd\footnote{\url{http://communicatie.leidenuniv.nl/huisstijl/basiselementen/lettertypen.html}}:
\begin{quote}
De Minion [familie] wordt voor lopende teksten gebruikt. Het lettertype Minion geldt ook voor brief- en rapportteksten in (laser-)printwerk. De complete familie kan worden toegepast. Cijfers in Minion worden altijd in mediaeval gezet (de expert varianten). De minimale corpsgrootte is 9 punten voor teksten in fullcolour en 8 punten voor gebruik in één kleur.
\end{quote}
\begin{quote}
De Bell [Centennial] familie wordt onder andere gebruikt voor advertentieteksten en koppen.
\end{quote}
In dit pakket zijn de lettertypen vervangen door 
\emph{Linux Libertine Legacy} (met \emph{mediaeval} of \emph{old style} nummers) en 
\emph{Bera Sans}.
Zoals te zien is in figuur~\ref{fig:logo} voldoet het Linux Libertine Legacy lettertype redelijk,
Bera Sans is echter niet zo'n goede vervanging voor Bell Centennial.
\begin{figure}[h]
	\centering
	\includegraphics[width=.5\textwidth]{Leiden.pdf}\\
	\hspace{-.5mm}{\fontsize{26.5}{32} \libertine Universiteit Leiden}\\
	\caption{Het zegel van de Universiteit Leiden vergeleken met het Linux Libertine Legacy lettertype.}
	\label{fig:logo}
\end{figure}

\newpage
\section{Gebruik}
\subsection{Opties}
\label{sec:opt}
Dit pakket heeft slechts een klein aantal opties:
\begin{itemize}
	\item \texttt{page} -- Zorgt voor een titelpagina in plaats van een kleine titel.
	\item \texttt{style} -- Stelt de lettertypen in volgens de huisstijl. Zie ook~\ref{sec:typeface}.
	\item \texttt{sans} -- Stelt alle lettertypen in op het standaard sans-serif lettertype (zie~\ref{sec:typeface}).
\end{itemize}
\subsection{Commands}
De volgende commando's worden gebruikt of beschikbaar gesteld door dit pakket:
\begin{itemize}
	\item \texttt{\textbackslash date} -- stel de datum in, valt terug op de huidige datum als weggelaten.
	\item \texttt{\textbackslash title} -- stel de titel in.
	\item \texttt{\textbackslash subtitle} -- stel de ondertitel in.
	\item \texttt{\textbackslash author} -- stel de auteur(s) in.
	\item \texttt{\textbackslash supervisor} -- stel de begeleider(s) in, wordt alleen getoond op de titelpagina.
	\item \texttt{\textbackslash course} -- stel het vak in.
	\item \texttt{\textbackslash faculty} -- stel de faculteit in.
	\item \texttt{\textbackslash maketitle} -- toon de titel(pagina).
\end{itemize}

\subsection{Overig}
Een aantal opties kunnen met de volgende commando's aangepast worden:
\begin{itemize}
	\item \texttt{\textbackslash titlepagetrue, \textbackslash titlepagefalse} -- verander the \texttt{page} optie.
\end{itemize}

\newpage
\section{Voorbeelden}
Hier zijn voorbeelden van beide weergaven.

\subsection{Simpele titel}
Deze titel is de standaard vervanging voor de titel.
\subsubsection{\LaTeX}
\begin{lstlisting}
\documentclass{article}
\usepackage{ULtitle}

\title{Titel}
\subtitle{Ondertitel}
\author{Auteur}
\course{Vaknaam}
\date{9 Augustus, 2012}
\faculty{Faculteit der faculteiten}

\begin{document}
\maketitle
\end{document}
\end{lstlisting}

\subsubsection{Resultaat}
\title{Titel}
\subtitle{Ondertitel}
\author{Auteur}
\course{Vaknaam}
\date{9 Augustus, 2012}
\faculty{Faculteit der faculteiten}
\begin{minipage}{\textwidth}
\maketitle
\end{minipage}

\newpage
\subsection{Titelpagina}
Dit titelblad wordt gemaakt met de \texttt{page} optie.
\subsubsection{\LaTeX}
\begin{lstlisting}
\documentclass{article}
\usepackage[page]{ULtitle}

\title{Titel}
\subtitle{Ondertitel}
\author{\emph{Door:}\\Auteur 1 -- s1234567\\Auteur 2 -- s1234567\\Auteur 3 -- s1234567\\Auteur 4 -- s1234567}
\supervisor{\emph{Begeleid door:}\\Begeleider}
\course{Vaknaam}
\faculty{Faculteit der faculteiten}

\begin{document}
\maketitle
\end{document}
\end{lstlisting}
\subsubsection{Result}
The next page is the result of the code above.
\author{\emph{Door:}\\Auteur 1 -- s1234567\\Auteur 2 -- s1234567\\Auteur 3 -- s1234567\\Auteur 4 -- s1234567}
\supervisor{\emph{Begeleid door:}\\Begeleider}
\date{\today}
\titlepagetrue
\newpage
\maketitle
\end{document}